Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => "/sidekiq"
  
  root to: "products#index"
  resources :order_lines, only: [:create, :destroy]
  devise_for :users
  devise_scope :user do
    get 'login', to: 'devise/sessions#new'
    delete 'logout', to: 'devise/sessions#destroy'
  end
  concern :commentable do
    resources :comments, only: [:create, :destroy, :update]
  end
  resources :users, only: [:show, :index], concerns: :commentable do
    member do
      get "edit_my_profile", to: "users#edit_my_profile"
      put "update_my_profile", to: "users#update_my_profile"
    end
  end
  resources :products, only: [:index, :show], concerns: :commentable do
    member do
      put "like", to: "products#like"
      put "dislike", to: "products#dislike"
    end
  end
  resources :orders, only: [:destroy, :update], concerns: :commentable do
  end
  namespace :admin do
    resources :products, concerns: :commentable
    resources :orders, only: [:index, :show]
    resources :product_logs, only: :index
    resources :users, concerns: :commentable do 
      member do
        get "edit_my_profile", to: "users#edit_my_profile"
        put "update_my_profile", to: "users#update_my_profile"
      end
    end
    get "comments/not_approved", to: "comments#not_approved"
    get "/" => "products#index"
  end
  resource :carts, only: [:show], path: 'cart'

  #API Namespace
  use_doorkeeper do
    skip_controllers :authorizations, :applications,
      :authorized_applications
  end
  namespace :api, defaults: {format: :json} do
    namespace :v1 do
      get "/" => "products#index"
      devise_for :users, controllers: {
        registrations: 'api/v1/users/registrations',
      }, skip: [:sessions, :password]
      resources :order_lines, only: [:create, :destroy]
      resources :products do
        member do
          put "like", to: "products#like"
          put "dislike", to: "products#dislike"
        end
      end
      resources :orders, only: [:destroy, :show, :index], concerns: :commentable do
        member do
          put "checkout", to: "orders#checkout"
        end
      end
      resource :carts, only: [:show], path: 'cart'
      resource :passwords do
        member do
          post "request_change", to: "passwords#request_change"
          post "reset", to: "passwords#reset"
        end
      end
    end
  end
end
