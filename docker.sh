rm -r tmp/pids/server.pid
bundle check || bundle install --jobs 10
bundle exec rails db:create
bundle exec rails db:migrate
bundle exec rails s -b '0.0.0.0'