FROM ruby:2.6.3-stretch
RUN apt-get update -qq && \
    apt-get install -y build-essential libpq-dev nodejs && \
    gem install bundler
RUN mkdir /merlos_snackstore
COPY Gemfile Gemfile.lock /merlos_snackstore/
WORKDIR /merlos_snackstore
RUN bundle install
COPY . /merlos_snackstore
RUN chmod +x ./docker.sh