# Merlos Snackstore | Jonathan Merlos | Applaudo Studios
App created for Applaudo Studios Trainee Program. 
This app is a beer store that contains all the basic features of a web ecommerce app.

* Ruby version : 2.6.3
* Rails version : 5.2.3

# System dependencies
* PostgreSQL.
* Run ```bundle install``` before starting the app to install all gem dependencies.

# Database creation and initializatio
* Set up your environment variables in a .env file inside the project root.
* Please run ```rails db:create && rails db:migrate && rails db:seed```

# Default users
* user: admin@merlos.com, password: admini, role: admin.
* user: regular@merlos.com, password: regular, role: regular.
* user: support@merlos.com, password: support, role: support.

# Running the app locally
* Run ```rails server``` to launch the app (Default port will be 3000).

# ENV variables
* Replace the following environment variables
```
# Database access
DEV_DATABASE=
DEV_USERNAME=
DEV_PASSWORD=
DEV_HOST=
DEV_PORT=
TEST_DATABASE=
PROD_DATABASE=
PROD_USERNAME=

# Mailing set-up
MAIL_USERNAME=
MAIL_PASSWORD=

# Stripe API Keys
STRIPE_DEV_PUBLISHABLE_KEY=
STRIPE_DEV_SECRET_KEY=

#Sidekiq
REDIS_URL=
```