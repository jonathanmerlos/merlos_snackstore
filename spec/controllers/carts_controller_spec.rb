# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CartsController, type: :controller do
  before :each do
    @user = create(:user)
    sign_in @user
    @order = create(:order)
  end

  describe 'GET #show' do
    it 'should success and render to show page' do
      get :show, params: {}, session: { order_id: @order.id }
      expect(response).to have_http_status(200)
      expect(response).to render_template :show
    end
  end
end
