# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CommentsController, type: :controller do
  before :each do
    @user = create(:user)
    sign_in @user
    @product = create(:product)
  end

  describe 'POST to #create' do
    it 'creates a comment' do
      comment_params = { comment: { user_id: @user.id, body: 'test comment', rating: '4' }, product_id: @product.id }
      expect { post :create, xhr: true, params: comment_params }.to change(Comment, :count).by(1)
    end
  end

  describe 'DELETE to #destroy' do
    before do
      @comment = Comment.create!(user_id: @user.id, body: 'test comment', rating: '4', commentable: @product)
    end

    it 'deletes a comment' do
      comment_params = { id: @comment.id, product_id: @product.id }
      expect { delete :destroy, xhr: true, params: comment_params }.to change(Comment, :count).by(-1)
    end
  end
end
