# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrderLinesController, type: :controller do
  before :each do
    @user = create(:user)
    sign_in @user
    @product = create(:product)
    @order = create(:order)
  end

  describe 'POST to #create' do
    it 'creates a new order line' do
      order_line_params = { order_line: { quantity: 1, product_id: @product.id } }
      expect { post :create, params: order_line_params, session: { order_id: @order.id } }.to change(OrderLine, :count).by(1)
    end
  end

  describe 'DELETE to #destroy' do
    it 'deletes an order line' do
      order_line_params = { id: @order.id }
      delete :destroy, params: order_line_params, session: { order_id: @order.id }
    end
  end
end
