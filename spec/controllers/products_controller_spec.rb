# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProductsController, type: :controller do
  describe 'GET #index' do
    before { get :index }

    it 'assigns active @products' do
      products = Product.active.page
      expect(assigns(:products)).to eq(products)
    end

    it { should render_template('index') }
    it { should respond_with(200) }
  end

  describe 'GET #show' do
    let!(:product) { create(:product) }

    it 'should success and render to show page' do
      get :show, params: { id: product.id }
      expect(response).to have_http_status(200)
      expect(response).to render_template :show
    end
  end
end
