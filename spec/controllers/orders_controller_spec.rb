# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrdersController, type: :controller do
  before :each do
    @user = create(:user)
    sign_in @user
  end

  describe 'POST to #create' do
    before do
      @order = create(:order)
      post :create, params: {}, session: { order_id: @order.id }
    end

    it 'updates order to completed' do
      expect(@order.reload.status).to eq('complete')
    end

    it { should redirect_to(root_path) }
    it { should respond_with(302) }
  end

  describe 'DELETE to #destroy' do
    before do
      @order = create(:order)
      order_params = { id: @order.id }
      delete :destroy, params: order_params, session: { order_id: @order.id }
    end

    it 'updates order to canceled' do
      expect(@order.reload.status).to eq('canceled')
    end

    it { should redirect_to(root_path) }
    it { should respond_with(302) }
  end
end
