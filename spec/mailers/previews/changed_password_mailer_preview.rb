# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/changed_password_mailer
class ChangedPasswordMailerPreview < ActionMailer::Preview
  def submission
    ChangedPasswordMailer.submission(User.take.id)
  end
end
