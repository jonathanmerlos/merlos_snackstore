# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/low_stock_mailer
class LowStockMailerPreview < ActionMailer::Preview
  def submission
    @product = Product.take
    @product.upvote_by(User.take)
    LowStockMailer.submission(@product.id)
  end
end
