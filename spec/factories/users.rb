# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { 'testadmin@merlos.com' }
    password { 'admini' }
    password_confirmation { 'admini' }
    admin { 0 }
  end
end
