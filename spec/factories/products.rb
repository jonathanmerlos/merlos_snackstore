# frozen_string_literal: true

require 'faker'
require 'securerandom'

FactoryBot.define do
  factory :product do
    sku { SecureRandom.uuid }
    name { "#{Faker::Beer.brand} #{Faker::Beer.name}" }
    description { "#{Faker::Beer.style}, #{Faker::Beer.alcohol}" }
    price { Faker::Commerce.price(1.00..50.00) }
    stock { rand(25..100) }
  end
end
