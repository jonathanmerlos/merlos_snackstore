# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Tag, type: :model do
  context 'associations' do
    it { should have_many(:product_tags) }
    it { should have_many(:products).through(:product_tags) }
  end

  context 'validations' do
    it { should validate_presence_of(:name) }
  end
end
