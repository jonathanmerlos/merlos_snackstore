# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProductLog, type: :model do
  context 'associations' do
    it { should belong_to(:admin).optional }
    it { should belong_to(:product).optional }
  end

  context 'validations' do
    it { should validate_presence_of(:product_id) }
    it { should validate_presence_of(:admin_id) }
  end
end
