# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Order, type: :model do
  subject { Order.new(status: 'open') }
  let(:product) { Product.find(1) }

  context 'associations' do
    it { should belong_to(:user).optional }
    it { should have_many(:order_lines) }
    it { should have_many(:products).through(:order_lines) }
    it { should have_many(:comments) }
  end

  context 'validations' do
    it { should validate_presence_of(:status) }
    it { should validate_numericality_of(:total) }

    it 'validates +product_still_available?+' do
      subject.order_lines.new(product_id: product.id, quantity: product.stock + 1)
      subject.save
      expect(subject).to_not be_valid
    end
  end

  context 'callbacks' do
    it 'triggers +decrease_product_stock+ on update' do
      product_stock = product.stock
      subject.order_lines.new(product_id: product.id, quantity: 3)
      subject.save
      subject.update(status: 'complete')
      expect(product.reload.stock).to eq(product_stock - 3)
    end
  end
end
