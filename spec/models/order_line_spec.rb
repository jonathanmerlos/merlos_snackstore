# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrderLine, type: :model do
  let(:product) { create(:product) }
  let(:order) { create(:order) }
  subject { OrderLine.create(product_id: product.id, order_id: order.id, quantity: 3) }

  context 'associations' do
    it { expect(subject).to belong_to(:order).optional }
    it 'should belong to product' do
      expect(OrderLine.reflect_on_association(:product).macro).to eq(:belongs_to)
    end
  end

  context 'validations' do
    it { should validate_numericality_of(:quantity) }

    it 'validates +out_of_stock?+' do
      subject.update(product_id: product.id, quantity: product.stock + 1)
      expect(subject).to_not be_valid
    end
  end

  context 'callbacks' do
    before do
      @order_total = subject.order.total
    end

    it 'calculates total before validation' do
      expect(subject.total).to eq(product.price * 3)
    end

    it 'updates order total on save' do
      subject.update(quantity: 2)
      expect(subject.order.total).to eq(@order_total - product.price)
    end

    it 'reduces order total on destroy' do
      order_line_total = subject.total
      subject.destroy
      expect(order.total).to eq(@order_total - order_line_total)
    end
  end
end
