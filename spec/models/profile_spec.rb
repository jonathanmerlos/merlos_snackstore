# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Profile, type: :model do
  context 'associations' do
    it { should belong_to(:user).optional }
  end

  context 'validations' do
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
    it { should validate_presence_of(:address) }
    it { should validate_presence_of(:phone) }
  end
end
