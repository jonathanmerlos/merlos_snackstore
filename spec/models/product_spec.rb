# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Product, type: :model do
  context 'scopes' do
    it 'applies a default scope to products by name ascending' do
      expect(Product.all.to_sql).to eq(Product.all.order(name: :asc).to_sql)
    end
  end
  context 'associations' do
    it { should have_many(:orders).through(:order_lines) }
    it { should have_many(:order_lines) }
    it { should have_many(:tags).through(:product_tags) }
    it { should have_many(:product_tags) }
    it { should have_many(:product_logs) }
    it { should have_many(:comments) }
  end

  context 'validations' do
    it { should validate_presence_of(:sku) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:description) }
    it { should validate_presence_of(:price) }
    it { should validate_presence_of(:stock) }
    it { should validate_uniqueness_of(:sku) }
    it { should validate_numericality_of(:price) }
    it { should validate_numericality_of(:stock) }
    it { should validate_numericality_of(:cached_votes_up) }
    it { should validate_numericality_of(:cached_votes_down) }
  end
end
