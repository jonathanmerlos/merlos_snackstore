require 'faker'
require 'securerandom'

User.create!(
  email: 'admin@merlos.com',
  password: 'admini',
  password_confirmation: 'admini',
  role: 1
  )

User.create!(
  email: 'regular@merlos.com',
  password: 'regular',
  password_confirmation: 'regular',
  role: 0
  )

User.create!(
  email: 'support@merlos.com',
  password: 'support',
  password_confirmation: 'support',
  role: 2
  )

User.find_each do |user|
  Profile.create(
    user_id: user.id,
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    address: Faker::Address.full_address,
    phone: Faker::PhoneNumber.phone_number
  )
end

30.times do
  Product.create(
    sku: SecureRandom.uuid,
    name: "#{Faker::Beer.brand} #{Faker::Beer.name}",
    description: "#{Faker::Beer.style}, #{Faker::Beer.alcohol}",
    price: Faker::Commerce.price(range: 1.00..50.00),
    stock: rand(25..100)
  )
end

10.times do
  Tag.create(
    name: "#{Faker::Beer.unique.hop}"
  )
end

Product.ids.each do |i|
  rand(1..3).times do
    t_id = Tag.ids.sample
    ProductTag.create(
      product_id: i,
      tag_id: t_id
    )
  end
end


20.times do
  order = Order.create(
    status: Order.status_options.values.sample
  )
  if order.status == 'complete'
    order.update(completed_at: Faker::Time.backward(days: 30), user_id: User.ids.sample)
  end
end

Order.ids.each do |i|
  rand(1..3).times do
    p_id = Product.ids.sample
    o_line = OrderLine.create(
      order_id: i,
      product_id: p_id,
      quantity: rand(1..5),
      price: Product.find(p_id).price
    )
    puts o_line.errors.messages[:quantity] unless o_line.errors.empty?
  end
end

puts 'Seed finished:'
puts "#{User.count} users created."
puts "#{Profile.count} profiles created."
puts "#{Product.count} products created."
puts "#{Tag.count} tags created."
puts "#{ProductTag.count} product_tags created."
puts "#{Order.count} orders created."
puts "#{OrderLine.count} order lines created."
