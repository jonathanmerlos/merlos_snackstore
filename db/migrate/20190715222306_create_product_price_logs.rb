class CreateProductPriceLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :product_price_logs do |t|
      t.references :product, foreign_key: true, null: false
      t.decimal :price_before, null: false, precision: 15, scale: 2
      t.decimal :price_after, null: false, precision: 15, scale: 2
      t.references :admin, index: true, foreign_key: { to_table: :users }
      t.timestamps
    end
  end
end
