class RenameProductPriceLogsTableToProductLogsTable < ActiveRecord::Migration[5.2]
  def change
    rename_table :product_price_logs, :product_logs
    remove_column :product_logs, :price_before
    remove_column :product_logs, :price_after
  end
end
