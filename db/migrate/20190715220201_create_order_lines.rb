class CreateOrderLines < ActiveRecord::Migration[5.2]
  def change
    create_table :order_lines do |t|
      t.references :order, foreign_key: {on_delete: :cascade}
      t.references :product, foreign_key: true, null: false
      t.integer :quantity, null: false, default: 1
      t.decimal :price, null: false, precision: 15, scale: 2
      t.decimal :total, default: 0, precision: 15, scale: 2
      t.timestamps
    end
  end
end
