class AddPositiveNumberConstraintToFields < ActiveRecord::Migration[5.2]
  def up
    execute <<-SQL
      ALTER TABLE products ADD CONSTRAINT price CHECK (price >= 0);
      ALTER TABLE products ADD CONSTRAINT stock CHECK (stock >= 0);
      ALTER TABLE orders ADD CONSTRAINT total CHECK (total >= 0);
      ALTER TABLE order_lines ADD CONSTRAINT price CHECK (price >= 0);
      ALTER TABLE order_lines ADD CONSTRAINT quantity CHECK (quantity >= 0);
      ALTER TABLE order_lines ADD CONSTRAINT total CHECK (total >= 0);
      ALTER TABLE product_price_logs ADD CONSTRAINT price_before CHECK (price_before >= 0);
      ALTER TABLE product_price_logs ADD CONSTRAINT price_after CHECK (price_after >= 0);
    SQL
  end
  def down
  execute <<-SQL
    ALTER TABLE products DROP CONSTRAINT price;
    ALTER TABLE products DROP CONSTRAINT stock;
    ALTER TABLE orders DROP CONSTRAINT total CHECK;
    ALTER TABLE order_lines DROP CONSTRAINT price CHECK;
    ALTER TABLE order_lines DROP CONSTRAINT quantity CHECK;
    ALTER TABLE order_lines DROP CONSTRAINT total CHECK;
    ALTER TABLE product_price_logs DROP CONSTRAINT price_before CHECK;
    ALTER TABLE product_price_logs DROP CONSTRAINT price_after CHECK;
  SQL
  end
end
