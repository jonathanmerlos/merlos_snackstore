class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :sku, null: false, index: {unique: true}
      t.string :name, null: false
      t.string :description, null: false
      t.decimal :price, null: false, precision: 15, scale: 2
      t.integer :stock, null: false, default: 0
      t.boolean :active, null: false, default: true
      t.integer :cached_votes_up, default: 0
      t.integer :cached_votes_down, default: 0
      t.timestamps
    end
  end
end
