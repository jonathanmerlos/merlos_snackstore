class AddOneToFiveRatingConstraint < ActiveRecord::Migration[5.2]
  def up
    execute <<-SQL
      ALTER TABLE comments ADD CONSTRAINT rating CHECK ((rating BETWEEN 1 AND 5) OR rating IS NULL);
    SQL
  end
  def down
  execute <<-SQL
    ALTER TABLE comments DROP CONSTRAINT rating;
  SQL
  end
end
