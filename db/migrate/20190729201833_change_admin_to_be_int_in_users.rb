class ChangeAdminToBeIntInUsers < ActiveRecord::Migration[5.2]
  def up
    add_column :users, :role, :integer, default: 0
    execute "UPDATE users SET role = CAST(admin AS INTEGER);"
    remove_column :users, :admin
  end
end
