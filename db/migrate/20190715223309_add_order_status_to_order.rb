class AddOrderStatusToOrder < ActiveRecord::Migration[5.2]
  def up
    execute <<-SQL
      CREATE TYPE status_options AS ENUM ('open', 'abandoned','canceled', 'complete');
    SQL
    add_column :orders, :status, :status_options, null: false
  end
  
  def down
    execute <<-SQL
      DROP TYPE status_options;
    SQL
    remove_column :orders, :status
  end
end
