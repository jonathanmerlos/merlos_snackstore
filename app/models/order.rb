# frozen_string_literal: true

# Order model
class Order < ApplicationRecord
  belongs_to :user, optional: true
  has_many :order_lines
  has_many :products, through: :order_lines
  has_many :comments, as: :commentable
  enum status_options: {
    open: 'open',
    abandoned: 'abandoned',
    canceled: 'canceled',
    complete: 'complete'
  }
  validates :status, presence: true
  validates :total, numericality: { greater_than_or_equal_to: 0 }
  validate :product_still_available?

  after_update :decrease_product_stock

  def total_cents
    (total * 100).to_i
  end

  private

  # Tests again whether at the moment of placing order if product is still available
  def product_still_available?
    order_lines.each do |order_line|
      next unless order_line.quantity > order_line.product_stock ||
                  !order_line.product_active ||
                  !order_line.product_deleted_at.nil?

      errors.add(:quantity, "There are not enough #{order_line.product_name} on stock")
      throw :abort
    end
  end

  # Decreases product stocks on completion of order
  def decrease_product_stock
    if status == 'complete'
      order_lines.each do |order_line|
        order_line.product.stock -= order_line.quantity
        order_line.product.save
      end
    end
  end
end
