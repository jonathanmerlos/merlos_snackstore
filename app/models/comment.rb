# frozen_string_literal: true

# Comment model
class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :commentable, polymorphic: true
  validates :user_id, :body, presence: true
  validates :rating, numericality: { in: 1..5 }, allow_nil: true
  delegate :email, to: :user, prefix: true
  delegate :full_name, to: :user, prefix: true, allow_nil: true

  def self.default_scope
    joins(:user).where('users.deleted_at IS NULL')
  end

  def self.approved
    where(approved: true)
  end

  def self.not_approved
    where(approved: false)
  end
end
