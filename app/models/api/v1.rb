# frozen_string_literal: true

module Api::V1
  def self.table_name_prefix
    'api_v1_'
  end
end
