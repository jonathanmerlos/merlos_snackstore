# frozen_string_literal: true

# Module to access current user from models
module Current
  thread_mattr_accessor :user
end
