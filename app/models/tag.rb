# frozen_string_literal: true

# Tag model
class Tag < ApplicationRecord
  has_many :product_tags
  has_many :products, through: :product_tags
  validates :name, presence: true
end
