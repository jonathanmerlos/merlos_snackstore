# frozen_string_literal: true

# Product model
class Product < ApplicationRecord
  acts_as_paranoid
  acts_as_votable
  paginates_per 12
  has_one_attached :image
  has_many :order_lines
  has_many :orders, through: :order_lines
  has_many :product_tags
  has_many :tags, through: :product_tags
  has_many :product_logs
  has_many :comments, as: :commentable
  validates :sku, presence: true, uniqueness: true
  validates :name, :description, :price, :stock, presence: true
  validates :price, :stock, :cached_votes_up, :cached_votes_down, numericality: { greater_than_or_equal_to: 0 }
  after_update :check_for_low_stock

  def image_path
    ActiveStorage::Blob.service.send(:path_for, image.key) if image.attached?
  end

  # Default scope to sort by name ascendingly
  def self.default_scope
    order(name: :asc)
  end

  # Scope because of soft delete
  def self.not_deleted
    where('deleted_at IS NULL')
  end

  # Scope for customer side
  def self.active
    where('active = true AND deleted_at IS NULL')
  end

  private

  def check_for_low_stock
    if stock.between?(1, 3)
      NotifyOnLowStockJob.perform_later(id) if cached_votes_up > 0
    end
  end
end
