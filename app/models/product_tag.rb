# frozen_string_literal: true

# Product Tag model
class ProductTag < ApplicationRecord
  belongs_to :product, optional: true
  belongs_to :tag, optional: true
end
