# frozen_string_literal: true

# Profile model
class Profile < ApplicationRecord
  belongs_to :user, optional: true
  validates :first_name, :last_name, :address, :phone, presence: true

  def full_name
    "#{first_name} #{last_name}"
  end
end
