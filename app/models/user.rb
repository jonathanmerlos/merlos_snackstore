# frozen_string_literal: true

# Devise User model
class User < ApplicationRecord
  acts_as_paranoid
  paginates_per 12
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :access_grants,
           class_name: 'Doorkeeper::AccessGrant',
           foreign_key: :resource_owner_id,
           dependent: :delete_all

  has_many :access_tokens,
           class_name: 'Doorkeeper::AccessToken',
           foreign_key: :resource_owner_id,
           dependent: :delete_all
  has_one :profile
  has_many :orders
  has_many :product_logs, foreign_key: :admin_id
  has_many :comments
  has_many :comments, as: :commentable
  delegate :first_name, :last_name, :phone, :address, to: :profile, prefix: true, allow_nil: true
  delegate :full_name, to: :profile

  enum role_options: {
    0 => 'regular',
    1 => 'admin',
    2 => 'support_user'
  }
  validates :role, numericality: { in: role_options.keys }

  def self.authenticate(email, password)
    user = User.find_for_authentication(email: email)
    user.try(:valid_password?, password) ? user : nil
  end

  # To avoid cost of migrating admin column(boolean) to role column(integer)
  def regular?
    role == 0
  end

  def admin?
    role == 1
  end

  def support?
    role == 2
  end

  def role_name
    case role
    when 0
      'Regular'
    when 1
      'Admin'
    when 2
      'Support'
    end
  end

  # Shows only those not soft deleted
  def self.not_deleted
    where('deleted_at IS NULL')
  end
end
