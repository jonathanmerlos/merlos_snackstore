# frozen_string_literal: true

# OrderLine model
class OrderLine < ApplicationRecord
  belongs_to :order, optional: true
  belongs_to :product, optional: true
  delegate :name, :stock, :deleted_at, :active, to: :product, prefix: true, allow_nil: true
  before_validation :calculate_total
  validates :product_id, :quantity, presence: true
  validates :price, :quantity, :total, numericality: { greater_than_or_equal_to: 0 }
  validate :out_of_stock?
  before_update :re_order_total
  after_save :update_order_total
  before_destroy :reduce_order_total
  delegate :name, to: :product, prefix: true, allow_nil: true

  private

  # It calculates total of order line
  def calculate_total
    self.price = product.price
    self.total = price * quantity
  end

  # Checks whether or not product was in stock to create order line
  def out_of_stock?
    if quantity > product.stock
      errors.add(:quantity, "There are not enough #{product.name} on stock")
      throw :abort
    end
  end

  # Decreases total in order before to re calculate it and add it up
  def re_order_total
    order.total -= total_was
  end

  # It updates order total
  def update_order_total
    order.total += total
    order.save
  end

  # Reduces order total when deleting order line
  def reduce_order_total
    order.total -= total
    order.save
  end
end
