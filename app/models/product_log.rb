# frozen_string_literal: true

# ProductLog model
class ProductLog < ApplicationRecord
  belongs_to :product, optional: true
  belongs_to :admin, class_name: :User, foreign_key: :admin_id, optional: true
  validates :product_id, :admin_id, presence: true
end
