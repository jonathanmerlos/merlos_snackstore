# frozen_string_literal: true

class UsersQuery
  attr_reader :relation

  def initialize(relation = User.all)
    @relation = relation
  end

  def with_first_name_like(first_name)
    return relation unless first_name.present?

    with_profile
      .where('profiles.first_name ILIKE ?', "%#{first_name}%")
  end

  private

  def with_profile
    relation.joins(:profile)
  end
end
