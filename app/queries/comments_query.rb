# frozen_string_literal: true

class CommentsQuery
  attr_reader :relation

  def initialize(relation = Comment.all)
    @relation = relation
  end

  def approved_and_mine(current_user)
    relation.where('approved = ? OR user_id = ?', true, current_user)
  end
end
