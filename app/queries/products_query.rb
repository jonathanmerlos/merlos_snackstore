# frozen_string_literal: true

class ProductsQuery
  attr_reader :relation

  def initialize(relation = Product.all)
    @relation = relation
  end

  def with_product_name_like(name)
    return relation unless name.present?

    relation.where('products.name ILIKE ?', "%#{name}%")
  end

  def with_tag_like(name)
    return relation unless name.present?

    with_tags
      .where('tags.name = ?', name)
  end

  def sort_by_popularity
    unscoped
      .order('cached_votes_up DESC, cached_votes_down ASC')
  end

  private

  def with_tags
    relation.joins(:tags)
  end

  def unscoped
    relation.unscoped.not_deleted
  end
end
