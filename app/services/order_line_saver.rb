# frozen_string_literal: true

class OrderLineSaver
  def initialize(order, item_params)
    @order = order
    @item_params = item_params
  end

  def call
    @line = @order.order_lines.where('product_id = ?', @item_params[:product_id])
    if @line.empty?
      @item_params[:order_id] = @order.id
      @line = @order.order_lines.new(@item_params)
    else
      @line.first.quantity += @item_params[:quantity].to_i
      @line.first.save
    end
  end
end
