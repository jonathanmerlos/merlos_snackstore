# frozen_string_literal: true

class CartSetter
  def initialize(user = nil, session_order_id = nil)
    @user = user
    @session_order_id = session_order_id
  end

  def call
    if !!@session_order_id
      Order.find(@session_order_id)
    elsif !!@user && @user.orders&.last&.open?
      @user.orders.last
    elsif !!@user
      @user.orders.new(status: 'open')
    elsif !@user || !@user.admin?
      Order.new(status: 'open')
    end
  end
end
