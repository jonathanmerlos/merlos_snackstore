# frozen_string_literal: true

class ProductChangeLogger
  def initialize(product, product_params, current_user)
    @product = product
    @product_params = product_params
    @current_user = current_user
  end

  def call
    if changed?
      @product.product_logs.create(
        product_id: @product.id,
        admin_id: @current_user.id
      )
    end
  end

  private

  def changed?
    @product.attributes = @product_params
    @product.changed?
  end
end
