# frozen_string_literal: true

class CartMerger
  def initialize(session_order, open_order)
    @session_order = session_order
    @open_order = open_order
  end

  def call
    @open_order.update(status: 'merged')
    @session_order.order_lines.each do |order_line|
      OrderLineSaver.new(@open_order, order_line)
    end
  end
end
