# frozen_string_literal: true

# Controller for order lines of order
class OrderLinesController < ApplicationController
  before_action :set_order
  before_action :authorize_user

  # POST /order_lines
  def create
    OrderLineSaver.new(@order, item_params).call
    if @order.save
      session[:order_id] = @order.id
      redirect_to products_path, notice: 'Product was added to the cart.'
    else
      redirect_to products_path, alert: @order.errors.full_messages
    end
  end

  # DELETE /order_lines/:id
  def destroy
    @line = @order.order_lines.find(params[:id])
    if @line.destroy && @order.save
      redirect_to carts_path, notice: 'Order Line was succesfully deleted.'
    else
      redirect_to carts_path, alert: @order.errors.full_messages
    end
  end

  private

  def authorize_user
    authorize OrderLine
  end

  # Filters strong parameters for order lines
  def item_params
    params.require(:order_line).permit(:quantity, :product_id)
  end

  # Defines order to current order
  def set_order
    @order = current_order
  end
end
