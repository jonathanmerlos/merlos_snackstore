# frozen_string_literal: true

# Controller for orders log display from admin
class Admin::OrdersController < ApplicationController
  include Commentable
  before_action :authenticate_user!
  before_action :authorize_user
  before_action :set_order, except: [:index]

  # GET /admin/orders
  def index
    @orders = Order.all
  end

  # GET /admin/orders/:id
  def show
    @commentable = @order
    prepare_comments
  end

  private

  def authorize_user
    authorize [:admin, Order]
  end

  # Defines order to work with according to parameters
  def set_order
    @order = Order.find(params[:id])
  end
end
