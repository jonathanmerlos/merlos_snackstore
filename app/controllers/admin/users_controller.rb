# frozen_string_literal: true

class Admin::UsersController < ApplicationController
  include Commentable
  include Filterable
  before_action :authenticate_user!
  before_action :authorize_user
  before_action :set_user, except: %i[new create index]
  before_action :set_role_options, only: %i[new edit]

  # GET /admin/users
  def index
    @users = User.not_deleted.page params[:page]
    @users = filter_resource(@users)
    @users = UserDecorator.decorate_collection(@users).includes(:profile)
  end

  def show
    @commentable = @user
    prepare_comments
  end

  def new
    @user = User.new
  end

  def edit; end

  def edit_my_profile
    @form = EditUserProfileForm.new(current_user)
  end

  def update_my_profile
    @form = EditUserProfileForm.new(edit_my_profile_params.to_unsafe_h)
    if @form.update
      redirect_to [:admin, current_user], notice: 'Your profile was successfully updated.'
    else
      redirect_to edit_my_profile_admin_user_path, notice: @user.errors.full_messages
    end
  end

  def update
    if @user.update(user_params)
      redirect_to [:admin, current_user], notice: 'User was successfully updated.'
    else
      redirect_to edit_admin_user_path, alert: @user.errors.full_messages
    end
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to admin_users_path, notice: 'User was successfully created.'
    else
      redirect_to new_admin_user_path, alert: @user.errors.full_messages
    end
  end

  # DELETE /admin/user/:id
  def destroy
    if @user.destroy
      redirect_to admin_users_path, notice: 'User was successfully destroyed.'
    else
      redirect_to admin_users_path, alert: @user.errors.full_messages
    end
  end

  private

  def authorize_user
    authorize [:admin, User]
  end

  def set_user
    @user = User.find(params[:id]).decorate
  end

  # Filters strong product params
  def user_params
    if current_user.admin?
      params.require(:user).permit(:email, :password, :password_confirmation, :role)
    elsif current_user.support?
      params.require(:user).permit(:email, :password, :password_confirmation)
    end
  end

  def set_role_options
    @role_options = User.role_options
    @role_options = [[@role_options[0], 0], [@role_options[2], 2]]
  end

  def search_params
    params.fetch(:search, {}).permit(:name, :tag)
  end

  # Filters filter(sorting) parameters
  def filter_param
    params.permit(:filter)
  end

  def edit_my_profile_params
    params.require(:edit_user_profile_form).permit(:email, :first_name, :last_name, :phone, :address)
  end
end
