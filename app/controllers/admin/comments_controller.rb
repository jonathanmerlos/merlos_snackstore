# frozen_string_literal: true

# Controller for admin view of comments comments
class Admin::CommentsController < ApplicationController
  def not_approved
    authorize [:admin, Comment]
    @comments = Comment.not_approved.includes(user: :profile)
    @comments = CommentDecorator.decorate_collection(@comments)
  end
end
