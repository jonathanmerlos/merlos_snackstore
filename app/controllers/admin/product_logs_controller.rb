# frozen_string_literal: true

# Controller for display of price changes in product
class Admin::ProductLogsController < ApplicationController
  before_action :authenticate_user!

  # GET /admin/product_logs
  def index
    authorize [:admin, ProductLog]
    @product_logs = ProductLog.all
  end
end
