# frozen_string_literal: true

# Controller for products display and CRUD from admin
class Admin::ProductsController < ApplicationController
  include Commentable
  include Filterable
  before_action :authenticate_user!
  before_action :authorize_user
  before_action :set_product, except: %i[index new create]
  before_action :set_tag, only: %i[index edit new update create]

  # GET /admin/products
  # /admin/ (admin root path)
  def index
    @products = Product.not_deleted.page params[:page]
    @products = filter_resource(@products)
    @products = ProductDecorator.decorate_collection(@products.with_attached_image)
    @order_line = current_order.order_lines.new
  end

  # GET /admin/products/:id
  def show
    @commentable = @product
    prepare_comments
  end

  # GET /admin/products/new
  def new
    @product = Product.new
  end

  # GET /admin/products/:id/edit
  def edit; end

  # POST /admin/products
  def create
    @product = Product.new(product_params)
    if @product.save
      @product.image.attach(product_params[:image]) if product_params[:image]
      redirect_to @product, notice: 'Product was successfully created.'
    else
      redirect_to new_admin_product_path, alert: @product.errors.full_messages
    end
  end

  # PUT /admin/products/:id
  def update
    ProductChangeLogger.new(@product, product_params, current_user).call
    if @product.update(product_params)
      if product_params[:image]
        @product.image.purge
        @product.image.attach(product_params[:image])
      end
      redirect_to @product, notice: 'Product was successfully updated.'
    else
      redirect_to edit_admin_product_path, alert: @product.errors.full_messages
    end
  end

  # DELETE /admin/products/:id
  def destroy
    @product.image.purge
    if @product.destroy
      redirect_to root_path, notice: 'Product was successfully destroyed.'
    else
      redirect_to root_path, alert: @product.errors.full_messages
    end
  end

  private

  def authorize_user
    authorize [:admin, Product]
  end

  # Sets product to work with based on id
  def set_product
    @product = Product.find(params[:id]).decorate
  end

  # Sets all tags for display
  def set_tag
    @tags = Tag.all
  end

  # Filters strong product params
  def product_params
    if current_user.admin?
      params.require(:product).permit(:sku, :name, :description, :price, :stock, :active, :image, tag_ids: [])
    elsif current_user.support?
      params.require(:product).permit(:name, :description, :stock, :active, :image, tag_ids: [])
    end
  end

  # Filters strong search parameters (not required)
  def search_params
    params.fetch(:search, {}).permit(:name, :tag)
  end

  # Filters filter(sorting) parameters
  def filter_param
    params.permit(:filter)
  end
end
