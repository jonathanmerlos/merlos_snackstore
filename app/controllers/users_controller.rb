# frozen_string_literal: true

class UsersController < ApplicationController
  include Commentable
  include Filterable
  before_action :authenticate_user!, except: %i[index show]
  before_action :authorize_user, except: %i[index show]
  before_action :set_user, except: %i[new create index]

  # GET /users
  def index
    @users = User.not_deleted.page params[:page]
    @users = filter_resource(@users)
    @users = UserDecorator.decorate_collection(@users).includes(:profile)
  end

  def show
    @commentable = @user
    prepare_comments
  end

  def edit_my_profile
    @form = EditUserProfileForm.new(current_user)
  end

  def update_my_profile
    @form = EditUserProfileForm.new(edit_my_profile_params.to_unsafe_h)
    if @form.update
      redirect_to current_user, notice: 'Your profile was successfully updated.'
    else
      redirect_to edit_my_profile_user_path, notice: @user.errors.full_messages
    end
  end

  private

  def authorize_user
    authorize User
  end

  def set_user
    @user = User.find(params[:id]).decorate
  end

  def search_params
    params.fetch(:search, {}).permit(:name, :tag)
  end

  def filter_param
    params.permit(:filter)
  end

  def edit_my_profile_params
    params.require(:edit_user_profile_form).permit(:email, :first_name, :last_name, :phone, :address)
  end
end
