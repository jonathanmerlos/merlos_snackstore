# frozen_string_literal: true

# General controller from which all other controllers inherit
class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  helper_method :current_order
  before_action :set_current_user

  # Sets current order depending on context
  def current_order
    CartSetter.new(current_user, session[:order_id]).call
  end

  # Handles record not found exception
  def record_not_found
    redirect_back(fallback_location: root_path, alert: "We couldn't find what you were looking for. Keep exploring!")
  end

  # Sets current user to an attribute in Current module
  # Done this way to have access to it from models
  def set_current_user
    Current.user = current_user
  end

  # Redirects to create profile page if user doesn't have a page
  def after_sign_in_path_for(_resource)
    if current_user.profile.nil?
      current_user.admin? ? edit_my_profile_admin_user_path(current_user) : edit_my_profile_user_path(current_user)
    else
      root_path
    end
  end

  # Prevents not authorized users to have access to unallowed features
  def user_not_authorized(exception)
    flash[:alert] = 'You are not authorized to perform this action.' unless ['index?', 'show?'].include? exception.query
    current_user&.regular? ? redirect_to(root_path) : redirect_to(admin_path)
  end
end
