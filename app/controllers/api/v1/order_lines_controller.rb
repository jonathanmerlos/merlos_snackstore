# frozen_string_literal: true

class Api::V1::OrderLinesController < Api::V1::ApplicationController
  before_action :set_order

  def create
    OrderLineSaver.new(@order, item_params).call
    if @order.save
      render json: ::OrderRepresenter.new(@order), status: :created, location: api_v1_carts_path
    else
      render json: { error: @order.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @line = @order.order_lines.find(params[:id])
    if @line.destroy && @order.save
      render json: ::OrderRepresenter.new(@order), location: api_v1_carts_path
    else
      render json: { error: @order.errors }, status: :unprocessable_entity
    end
  end

  private

  def item_params
    params.require(:order_line).permit(:quantity, :product_id)
  end

  def set_order
    @order = current_order
  end
end
