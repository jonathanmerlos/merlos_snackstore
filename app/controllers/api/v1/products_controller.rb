# frozen_string_literal: true

class Api::V1::ProductsController < Api::V1::ApplicationController
  include Filterable
  before_action :doorkeeper_authorize!, except: %i[index show]
  before_action :set_product, except: %i[index create]

  def index
    @products = ::Product.active.page(params[:page])
    @products = filter_resource(@products)
    @products = ::ProductRepresenter.for_collection.new(@products.includes(:tags, :comments).with_attached_image)
    render json: { products: @products }, exclude: [:comments]
  end

  def show
    if @product.active && @product.deleted_at.nil?
      render json: ::ProductRepresenter.new(@product)
    else
      render json: { error: { id: "Record couldn't be found" } }, status: :not_found
    end
  end

  def create
    @product = ::Product.new(product_params)
    if @product.save
      @product.image.attach(product_params[:image]) if product_params[:image]
      render json: ::ProductRepresenter.new(@product), status: :created, location: @product
    else
      render json: { error: @product.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @product.update(product_params)
      if product_params[:image]
        @product.image.purge
        @product.image.attach(product_params[:image])
      end
      render json: ::ProductRepresenter.new(@product)
    else
      render json: { error: @product.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @product.image.purge
    if @product.destroy
      render json: ::ProductRepresenter.new(@product), location: @product
    else
      render json: { error: @product.errors }, status: :unprocessable_entity
    end
  end

  def like
    if @product.upvote_by(current_user)
      render json: ::ProductRepresenter.new(@product), location: @product
    else
      render json: { error: @product.errors }, status: :unprocessable_entity
    end
  end

  def dislike
    if @product.downvote_by(current_user)
      render json: ::ProductRepresenter.new(@product), location: @product
    else
      render json: { error: @product.errors }, status: :unprocessable_entity
    end
  end

  private

  def set_product
    @product = ::Product.find(params[:id])
  end

  def set_tag
    @tags = Tag.all
  end

  def product_params
    params.require(:product).permit(:sku, :name, :description, :price, :stock, :active, :image, tag_ids: [])
  end

  def search_params
    params.fetch(:search, {}).permit(:name, :tag)
  end

  def filter_param
    params.permit(:filter)
  end
end
