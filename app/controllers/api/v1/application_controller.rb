# frozen_string_literal: true

class Api::V1::ApplicationController < ActionController::API
  respond_to :json
  helper_method :current_user
  helper_method :current_user
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  def current_user
    User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
  end

  def current_order
    CartSetter.new(current_user).call
  end

  def current_order
    if doorkeeper_token && !Order.where('user_id = ? AND status = ?', current_user.id, 'open').empty?
      Order.where('user_id = ? AND status = ?', current_user.id, 'open').last
    elsif doorkeeper_token
      Order.new(status: 'open', user_id: current_user.id)
    end
  end

  private

  def record_not_found
    render json: { error: { id: "Record couldn't be found" } }, status: :not_found
  end
end
