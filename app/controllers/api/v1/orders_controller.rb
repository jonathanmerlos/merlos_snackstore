# frozen_string_literal: true

class Api::V1::OrdersController < Api::V1::ApplicationController
  rescue_from Stripe::CardError, with: :catch_exception
  before_action :doorkeeper_authorize!, except: [:destroy]
  before_action :set_order, except: [:index]

  def index
    @orders = ::OrderRepresenter.for_collection.new(current_user.orders.order(:id).includes(order_lines: :product))
    render json: { orders: @orders }
  end

  def checkout
    return render json: { warning: { checkout: 'Order has already been completed' } }, status: :conflict if @order.status == 'complete'

    if StripeCharger.new(charges_params, current_user).call &&
       @order.update(status: 'complete', user_id: current_user.id, completed_at: Time.now)
      render json: ::OrderRepresenter.new(@order), status: :created, location: @product
    else
      render json: { error: @order.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @order.update(status: 'canceled')
      render json: ::OrderRepresenter.new(@order)
    else
      render json: { error: @order.errors }, status: :unprocessable_entity
    end
  end

  def show
    render json: ::OrderRepresenter.new(@order)
  end

  private

  def set_order
    @order = Order.find(params[:id])
  end

  def charges_params
    params.permit(:stripeEmail, :stripeToken, :id)
  end

  def catch_exception(exception)
    flash[:error] = exception.message
  end
end
