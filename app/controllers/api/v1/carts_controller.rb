# frozen_string_literal: true

class Api::V1::CartsController < Api::V1::ApplicationController
  def show
    render json: ::OrderRepresenter.new(current_order)
  end
end
