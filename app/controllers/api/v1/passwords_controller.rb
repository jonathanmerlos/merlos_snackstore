# frozen_string_literal: true

class Api::V1::PasswordsController < Api::V1::ApplicationController
  def request_change
    if params[:email].blank?
      render json: { error: 'Email not present' }
    else
      user = User.find_by_email(params[:email].downcase)
      user.send_reset_password_instructions if user.present?
      render json: { success: 'An email has been sent with instructions if email was found!' }
    end
  end

  def reset
    return render json: { error: 'Token not present' } unless params[:reset_password_token]

    token = params[:reset_password_token].to_s
    user = User.with_reset_password_token(token)
    if user.present?
      if user.reset_password(password_params[:password], password_params[:password_confirmation])
        NotifyChangeOfPasswordJob.perform_later(user.id)
        render json: { success: 'Password was reset succesfully' }, status: :ok
      else
        render json: { error: user.errors.full_messages }, status: :unprocessable_entity
      end
    end
  end

  private

  def password_params
    params.require(:passwords).permit(:password, :password_confirmation)
  end
end
