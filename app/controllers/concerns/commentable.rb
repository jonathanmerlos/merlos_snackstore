# frozen_string_literal: true

# Module to include basic methods for controllers who require comments
module Commentable
  def prepare_comments
    @comments = @commentable.comments.includes(user: :profile)
    @comments = CommentsQuery.new(@comments).approved_and_mine(current_user) unless user_signed_in? && current_user.admin?
    @comments = CommentDecorator.decorate_collection(@comments)
    @comment = Comment.new
  end
end
