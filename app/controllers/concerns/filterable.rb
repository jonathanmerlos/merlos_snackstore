# frozen_string_literal: true

# Module to include basic methods for controllers who require resources to be filtered
module Filterable
  def filter_resource(resource)
    if search_params
      if resource.first.is_a?(User)
        resource = UsersQuery.new(resource).with_first_name_like(search_params[:name])
      elsif resource.first.is_a?(Product)
        resource = ProductsQuery.new(resource).with_product_name_like(search_params[:name])
        resource = ProductsQuery.new(resource).with_tag_like(search_params[:tag])
      end
    end
    if filter_param.present? && filter_param['filter'] == 'vote'
      resource = ProductsQuery.new(resource).sort_by_popularity
    end
    resource
  end
end
