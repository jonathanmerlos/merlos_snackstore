# frozen_string_literal: true

# Controller for products display
class ProductsController < ApplicationController
  include Commentable
  include Filterable
  before_action :authenticate_user!, except: %i[index show]
  before_action :authorize_user
  before_action :set_product, except: [:index]
  before_action :set_tag, only: [:index]
  before_action :set_new_order_line, only: %i[index show]

  # GET /products
  # / (root path)
  def index
    @products = Product.active.page(params[:page])
    @products = filter_resource(@products)
    @products = ProductDecorator.decorate_collection(@products.with_attached_image)
  end

  # GET /products/:id
  def show
    @commentable = @product
    prepare_comments
  end

  # PUT /products/:id/like
  def like
    @product.upvote_by(current_user)
    render 'vote.js'
  end

  # PUT /products/:id/dislike
  def dislike
    @product.downvote_by(current_user)
    render 'vote.js'
  end

  private

  def authorize_user
    authorize Product
  end

  # Sets a new order line for possible shopping
  def set_new_order_line
    @order_line = current_order.order_lines.new
  end

  # Sets product to work with based on id
  def set_product
    @product = Product.find(params[:id]).decorate
  end

  # Sets all tags for display
  def set_tag
    @tags = Tag.all
  end

  # Filters strong search parameters (not required)
  def search_params
    params.fetch(:search, {}).permit(:name, :tag)
  end

  # Filters filter(sorting) parameters
  def filter_param
    params.permit(:filter)
  end
end
