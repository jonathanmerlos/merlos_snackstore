# frozen_string_literal: true

# Controller for comments
class CommentsController < ApplicationController
  include Commentable
  before_action :set_commentable
  before_action :set_comment, only: %i[destroy update]
  before_action :authorize_user, except: [:create]

  # POST /comments
  def create
    authorize Comment
    cp = comments_params
    cp[:approved] = false if @commentable.instance_of?(User)
    @comment = @commentable.comments.new(cp)
    if @comment.save
      prepare_comments
    else
      redirect_to @commentable, alert: @comment.errors.full_messages
    end
  end

  # DELETE /comments/:id
  def destroy
    if @comment.destroy
      @comments = @commentable.comments.includes(user: :profile)
      prepare_comments_from_referer
      @comments = CommentDecorator.decorate_collection(@comments)
    else
      redirect_to @commentable, alert: @comment.errors.full_messages
    end
  end

  # PUT /comments/:id
  def update
    if @comment.update(approved: true)
      @comments = @commentable.comments.includes(user: :profile)
      prepare_comments_from_referer
      @comments = CommentDecorator.decorate_collection(@comments)
    else
      redirect_to @commentable, alert: @comment.errors.full_messages
    end
  end

  private

  def authorize_user
    authorize @comment
  end

  def prepare_comments_from_referer
    if request.referer.split('/').last == 'not_approved'
      @comments = @comments.not_approved
    elsif current_user.regular?
      @comments = CommentsQuery.new(@comments).approved_and_mine(current_user)
    end
  end

  # Filters strong parameters for comment
  def comments_params
    params.require(:comment).permit(:user_id, :body, :rating, :approved)
  end

  # Sets comment to work with
  def set_comment
    @comment = Comment.find(params[:id])
  end

  # Takes context based on url path variables
  def set_commentable
    resource, id = request.path.split('/')[1, 2]
    @commentable = resource.singularize.classify.constantize.find(id)
  end
end
