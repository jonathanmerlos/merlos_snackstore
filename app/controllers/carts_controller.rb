# frozen_string_literal: true

# Controller for shopping cart
class CartsController < ApplicationController
  include Commentable

  # GET /carts
  # GET /cart
  def show
    authorize current_order
    @order_lines = current_order.order_lines
    @order = current_order
    @commentable = current_order
    prepare_comments
  end
end
