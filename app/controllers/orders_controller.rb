# frozen_string_literal: true

# Controller for orders (classified as orders after cart is placed)
class OrdersController < ApplicationController
  rescue_from Stripe::CardError, with: :catch_exception
  before_action :authenticate_user!, except: [:destroy]
  before_action :authorize_user, except: [:destroy]
  before_action :set_order, only: [:destroy]

  # PATCH /order/:id/
  def update
    if StripeCharger.new(charges_params, current_user).call &&
       current_order.update(status: 'complete', user_id: current_user.id, completed_at: Time.now)
      session.delete(:order_id)
      redirect_to root_path, notice: 'Order was successfully placed.'
    else
      redirect_to carts_path, alert: current_order.errors.full_messages
    end
  end

  # DELETE /orders/:id (doesn't destroy but just sets status as canceled)
  def destroy
    if current_order.update(status: 'canceled')
      session.delete(:order_id)
      redirect_to root_path, notice: 'Order was successfully deleted.'
    else
      redirect_to carts_path, alert: current_order.errors.full_messages
    end
  end

  private

  def authorize_user
    authorize Order
  end

  # Defines order to work with according to parameters
  def set_order
    @order = Order.find(params[:id])
  end

  def charges_params
    params.permit(:stripeEmail, :stripeToken, :id)
  end

  def catch_exception(exception)
    render json: { error: exception.message }
  end
end
