# frozen_string_literal: true

class ProfileDecorator < Draper::Decorator
  delegate_all

  def full_name
    full_name = first_name + ' ' + last_name
    full_name.strip.empty? ? 'Not Profiled' : full_name
  end

  def address
    address.strip.empty? || address.nil? ? 'No defined address' : address
  end

  def phone
    phone.strip.empty? || phone.nil? ? 'No defined phone' : address
  end
end
