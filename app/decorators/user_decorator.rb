# frozen_string_literal: true

class UserDecorator < Draper::Decorator
  # decorates_association :comments
  decorates_association :profile
  delegate_all

  def self.collection_decorator_class
    PaginatingDecorator
  end

  def time_ago
    h.time_ago_in_words(created_at)
  end
end
