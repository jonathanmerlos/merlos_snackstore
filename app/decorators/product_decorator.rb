# frozen_string_literal: true

class ProductDecorator < Draper::Decorator
  # decorates_association :comments
  delegate_all

  def self.collection_decorator_class
    PaginatingDecorator
  end

  def active?
    active ? 'Active' : 'Inactive'
  end
end
