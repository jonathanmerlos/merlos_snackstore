# frozen_string_literal: true

class CommentDecorator < Draper::Decorator
  delegate_all

  def time_ago
    h.time_ago_in_words(created_at)
  end

  def approved?
    comment.approved ? 'Approved' : 'Pending'
  end
end
