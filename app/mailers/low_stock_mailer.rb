# frozen_string_literal: true

class LowStockMailer < ApplicationMailer
  def submission(product_id)
    @product = Product.find(product_id)
    @user = @product.get_likes.order(updated_at: :desc).first.voter
    mail(to: @user.email, subject: "#{@product.name} has only #{@product.stock} left on stock!")
  end
end
