# frozen_string_literal: true

class ChangedPasswordMailer < ApplicationMailer
  def submission(user_id)
    @user = User.find(user_id)
    mail(to: @user.email, subject: 'Your password was succesfully changed!')
  end
end
