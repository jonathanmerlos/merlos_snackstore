# frozen_string_literal: true

# Not used application mailer
class ApplicationMailer < ActionMailer::Base
  default from: 'merlos@merlos.com'
  layout 'mailer'
end
