# frozen_string_literal: true

class Admin::OrderPolicy
  attr_reader :user, :product

  def initialize(user, order)
    @user = user
    @product = order
  end

  def index?
    user.admin?
  end

  def show?
    user.admin?
  end
end
