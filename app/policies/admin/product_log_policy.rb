# frozen_string_literal: true

class Admin::ProductLogPolicy
  attr_reader :user, :product

  def initialize(user, product_log)
    @user = user
    @product_log = product_log
  end

  def index?
    user.admin?
  end
end
