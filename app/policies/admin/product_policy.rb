# frozen_string_literal: true

class Admin::ProductPolicy
  attr_reader :user, :product

  def initialize(user, product)
    @user = user
    @product = product
  end

  def index?
    !user.regular?
  end

  def show?
    !user.regular?
  end

  def new?
    user.admin?
  end

  def create?
    user.admin?
  end

  def edit?
    !user.regular?
  end

  def update?
    !user.regular?
  end

  def destroy?
    user.admin?
  end
end
