# frozen_string_literal: true

class Admin::CommentPolicy < ApplicationPolicy
  def initialize(user, comment)
    @user = user
    @comment = comment
  end

  def not_approved?
    !user.regular?
  end
end
