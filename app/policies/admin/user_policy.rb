# frozen_string_literal: true

class Admin::UserPolicy
  attr_reader :user, :product

  def initialize(user, user_to_work)
    @user = user
    @product = user_to_work
  end

  def index?
    !user.regular?
  end

  def show?
    !user.regular?
  end

  def new?
    !user.regular?
  end

  def create?
    !user.regular?
  end

  def edit?
    user.admin?
  end

  def update?
    user.admin?
  end

  def destroy?
    user.admin?
  end

  def edit_my_profile?
    !user.regular?
  end

  def update_my_profile?
    !user.regular?
  end
end
