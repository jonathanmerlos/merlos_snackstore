# frozen_string_literal: true

class OrderPolicy
  attr_reader :user, :product
  def initialize(user, order)
    @user = user
    @order = order
  end

  def update?
    user.regular?
  end

  def destroy?
    !user || user.regular?
  end

  def show?
    !user || user.regular?
  end
end
