# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  def initialize(user, user_to_work)
    @user = user
    @product = user_to_work
  end

  def edit_my_profile?
    user&.regular?
  end

  def update_my_profile?
    user&.regular?
  end
end
