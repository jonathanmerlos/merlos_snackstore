# frozen_string_literal: true

class CommentPolicy < ApplicationPolicy
  def initialize(user, comment)
    @user = user
    @comment = comment
  end

  def create?
    !!user
  end

  def update?
    !user.regular?
  end

  def destroy?
    user.admin? || @comment.user == user
  end
end
