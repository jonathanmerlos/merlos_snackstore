# frozen_string_literal: true

class ProductPolicy
  attr_reader :user, :product

  def initialize(user, product)
    @user = user
    @product = product
  end

  def index?
    !user || user.regular?
  end

  def show?
    !user || user.regular?
  end

  def like?
    user.regular?
  end

  def dislike?
    user.regular?
  end
end
