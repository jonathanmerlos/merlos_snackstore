# frozen_string_literal: true

class OrderLinePolicy < ApplicationPolicy
  def initialize(user, order_line)
    @user = user
    @order_line = order_line
  end

  def create?
    !user || user.regular?
  end

  def destroy?
    !user || user.regular?
  end
end
