# frozen_string_literal: true

class OrderRepresenter < Representable::Decorator
  include Representable::JSON
  property :id
  property :total
  property :status

  collection :order_lines, decorator: OrderLineRepresenter
end
