# frozen_string_literal: true

class OrderLineRepresenter < Representable::Decorator
  include Representable::JSON
  property :id
  property :quantity
  property :product_name
  property :price
  property :total
end
