# frozen_string_literal: true

class ProductRepresenter < Representable::Decorator
  include Representable::JSON
  property :id
  property :name
  property :description
  property :price
  property :stock
  property :cached_votes_up, as: :likes
  property :cached_votes_down, as: :dislikes
  property :image_path

  collection :tags, decorator: TagRepresenter
  collection :comments, decorator: CommentRepresenter
end
