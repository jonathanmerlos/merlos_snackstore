# frozen_string_literal: true

class TagRepresenter < Representable::Decorator
  include Representable::JSON
  property :id
  property :name
end
