# frozen_string_literal: true

class CommentRepresenter < Representable::Decorator
  include Representable::JSON
  property :id
  property :user_full_name
  property :user_email
  property :body
  property :rating
  property :created_at, as: :date
end
