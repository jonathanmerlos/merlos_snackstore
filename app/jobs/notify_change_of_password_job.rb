# frozen_string_literal: true

class NotifyChangeOfPasswordJob < ApplicationJob
  queue_as :default

  def perform(user_id)
    ChangedPasswordMailer.submission(user_id).deliver
  end
end
