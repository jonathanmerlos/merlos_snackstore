# frozen_string_literal: true

class NotifyOnLowStockJob < ApplicationJob
  queue_as :default

  def perform(product_id)
    LowStockMailer.submission(product_id).deliver
  end
end
