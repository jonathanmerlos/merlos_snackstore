# frozen_string_literal: true

class EditUserProfileForm
  include ActiveModel::Model

  attr_accessor :email
  attr_accessor :first_name
  attr_accessor :last_name
  attr_accessor :phone
  attr_accessor :address

  validates :email, presence: true
  validates :first_name, :last_name, :phone, :address, presence: true

  def initialize(edit_params)
    edit_params = convert_to_hash(edit_params) unless edit_params.is_a?(Hash)
    super(edit_params)
  end

  def update
    return false unless valid?

    user = User.find(Current.user.id)
    user.update(email: email)
    user.build_profile if user.profile.nil?
    user.profile.update(first_name: first_name, last_name: last_name, phone: phone, address: address)
  end

  private

  def convert_to_hash(current_user)
    {
      email: current_user.email,
      first_name: current_user.profile_first_name,
      last_name: current_user.profile_last_name,
      phone: current_user.profile_phone,
      address: current_user.profile_address
    }
  end
end
